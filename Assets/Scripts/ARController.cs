﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ARController : MonoBehaviour
{
    [Header("Data")]
    public WebCamDetector detector;

    [Header("Display")]
    public bool showGraphics = true;
    public bool showBoundingBoxes;
    public SmoothTransform display;
    public Image displayImage;
    public Image letterHint;
    public AudioSource audioSource;

    [Header("Letters")]
    public LetterData[] letters;

    private ImageData selectedImage;
    private LetterData cachedLetter;
    private LetterData recognizedLetter;
    private int resetImageCountdown;

    private Camera mainCamera;

    [Header("Time")]
    public float hideTimeout;
    private float hideCountdown;

    private void Start()
    {
        mainCamera = Camera.main;
    }

    public void Update()
    {
        if (!showGraphics)
            return;

        if (hideCountdown > 0)
        {
            hideCountdown -= Time.deltaTime;
        }
        else
        {
            if(display.gameObject.activeSelf)
                display.gameObject.SetActive(false);
            if(!letterHint.gameObject.activeSelf)
                letterHint.gameObject.SetActive(true);
        }

        if (detector.results == null || detector.results.Count() < 1)
            return;

        recognizedLetter = null;

        YOLOHandler.ResultBox box;
        for (int i = 0; i < detector.results.Count(); i++)
        {
            if (!showGraphics)
                return;

            if (recognizedLetter != null)
                break;

            box = detector.results.ElementAt(i);
            if (box.classes[box.bestClassIdx] > detector.MinBoxConfidence)
            {
                for (int j = 0; j < letters.Length; j++)
                {
                    if (letters[j].name.Equals(detector.classesNames[box.bestClassIdx]))
                    {
                        recognizedLetter = letters[j];
                        break;
                    }
                }

                if (recognizedLetter == null)
                    continue;

                box.rect.width *= (float)Screen.height / detector.displayingTex.height;
                box.rect.height *= (float)Screen.height / detector.displayingTex.height;

                box.rect.x -= ((Screen.height - Screen.width) / 2f) * (detector.displayingTex.height / (float)Screen.height);
                box.rect.x *= detector.displayingTex.height / (float)Screen.width;
                box.rect.y *= detector.displayingTex.height / (float)Screen.width;

                hideCountdown = hideTimeout;
                if (letterHint.gameObject.activeSelf)
                    letterHint.gameObject.SetActive(false);

                ShowGraphics(recognizedLetter, box.rect);

                //Debug.Log("Drawing rect " + classesNames[box.bestClassIdx]);
                //Debug.Log("Drawing rect " + box.rect);

                //GUI.Label(box.rect, detector.classesNames[box.bestClassIdx] + "(" + box.classes[box.bestClassIdx].ToString("0.0") + ")");
                //Utils.DrawRect(box.rect, 1, Color.yellow);
            }
        }
    }

    public void OnGUI()
    {
        if (!showBoundingBoxes)
            return;

        if (detector.results == null || detector.results.Count() < 1)
            return;

        //Debug.Log("Screen " + Screen.width + " | " + Screen.height);
        //Debug.Log("Tex " + displayingTex.width + " | " + displayingTex.height);

        detector.results.ForEach(box => {
            if (box.classes[box.bestClassIdx] > detector.MinBoxConfidence)
            {
                box.rect.width *= (float)Screen.height / detector.displayingTex.height;
                box.rect.height *= (float)Screen.height / detector.displayingTex.height;

                box.rect.x -= ((Screen.height - Screen.width) / 2f) * (detector.displayingTex.height / (float)Screen.height);
                box.rect.x *= detector.displayingTex.height / (float)Screen.width;
                box.rect.x *= 2;
                box.rect.y *= detector.displayingTex.height / (float)Screen.width;
                box.rect.y *= 2;

                //Debug.Log("Drawing rect " + classesNames[box.bestClassIdx]);
                //Debug.Log("Drawing rect " + box.rect);

                GUI.Label(box.rect, detector.classesNames[box.bestClassIdx] + "(" + box.classes[box.bestClassIdx].ToString("0.0") + ")");
                Utils.DrawRect(box.rect, 1, Color.yellow);
            }
        });
    }

    private void ShowGraphics(LetterData letter, Rect rect)
    {
        //Debug.Log("Showing graphics");
        //Debug.Log("Rect X:" + rect["x"] + " | Y:" + rect["y"] + " | W:" + rect["w"] + " | H:" + rect["h"]);

        var xMin = rect.x;
        var yMin = rect.y;
        var xMax = rect.x + rect.width;
        var yMax = rect.y + rect.height;

        //var pos = GetPosition((xMin + xMax) / 2 * Screen.width, (yMin + yMax) / 2 * Screen.height);
        //Debug.Log("Screen position x: " + ((xMin + xMax) / 2) + " | y: " + ((yMin + yMax) / 2));
        Vector3 pos = mainCamera.ScreenToWorldPoint(new Vector3((xMin + xMax) / 2, Screen.height - ((yMin + yMax) / 2), 10));
        float scale = ((rect.width + rect.height) / 2) / (Screen.width * 0.7f);

        //Debug.Log("Position " + pos);

        display.SetTargetPosition(pos);
        display.SetTargetScale(new Vector2(scale, scale));

        if (cachedLetter != null && letter != cachedLetter)
        {
            cachedLetter = letter;
            selectedImage = null;
        }

        if (cachedLetter == null)
            cachedLetter = letter;

        ImageData imageData = cachedLetter.imageData[Random.Range(0, letter.imageData.Length)];
        if (selectedImage == null)
        {
            selectedImage = imageData;
        }
        else
        {
            imageData = selectedImage;
        }

        displayImage.sprite = imageData.sprite;
        audioSource.clip = imageData.sound;
    }

    public void PlaySound()
    {
        audioSource.Stop();
        audioSource.Play();
    }
}
