﻿using System.Collections;
using System.Collections.Generic;
using Unity.Barracuda;
using UnityEngine;
using UnityEngine.UI;

public class ModelProcessor : MonoBehaviour
{
    [Header("Display")]
    public RawImage image;

    [Header("Model")]
    public NNModel modelAsset;
    private Model m_RuntimeModel;

    private IWorker worker;
    private Tensor tensor;

    private WebCamTexture webCam;
    private Texture2D texture;

    void Start()
    {
        m_RuntimeModel = ModelLoader.Load(modelAsset);

        worker = WorkerFactory.CreateWorker(WorkerFactory.Type.ComputePrecompiled, m_RuntimeModel);

        webCam = new WebCamTexture();
        webCam.Play();

        texture = new Texture2D(webCam.width, webCam.width);
        image.texture = texture;

        StartCoroutine(MLUpdate());
    }

    private IEnumerator MLUpdate()
    {
        WaitForSeconds wait = new WaitForSeconds(0.5f);

        while (true)
        {
            texture.UpdateExternalTexture(webCam.GetNativeTexturePtr());
            image.texture = texture;

            tensor = new Tensor(texture, 3);
            worker.Execute(tensor);

            Tensor output = worker.PeekOutput("output");
            Debug.Log(output.ToString());

            tensor.Dispose();

            yield return wait;
        }
    }

    private void OnDestroy()
    {
        worker.Dispose();
    }
}
