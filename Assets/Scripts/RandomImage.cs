﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomImage : MonoBehaviour
{
    public Image targetImage;
    public AudioSource targetAudioSource;
    public ImageData[] imageData;

    private void OnEnable()
    {
        int random = Random.Range(0, imageData.Length);

        // choose one sprite and set as image
        targetImage.sprite = imageData[random].sprite;

        // set sound
        targetAudioSource.clip = imageData[random].sound;
    }

    public void PlaySound()
    {
        targetAudioSource.Play();
    }
}
