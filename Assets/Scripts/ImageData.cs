﻿using System;
using UnityEngine;

[Serializable]
public class ImageData
{
    public string name;
    public Sprite sprite;
    public AudioClip sound;
}
