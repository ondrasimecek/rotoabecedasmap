﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class LetterData
{
    public string name;
    public Sprite letterHint;
    public ImageData[] imageData;
}
