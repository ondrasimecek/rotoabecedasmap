﻿using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Unity.Barracuda;
using UnityEngine.Profiling;

[RequireComponent(typeof(OnGUICanvasRelativeDrawer))]
public class WebCamDetector : MonoBehaviour
{
    [Tooltip("File of YOLO model. If you want to use another than YOLOv2 tiny, it may be necessary to change some const values in YOLOHandler.cs")]
    public NNModel modelFile;
    [Tooltip("Text file with classes names separated by coma ','")]
    public TextAsset classesFile;

    [Tooltip("RawImage component which will be used to draw resuls.")]
    public RawImage imageRenderer;

    [Range(0.05f, 1f)]
    [Tooltip("The minimum value of box confidence below which boxes won't be drawn.")]
    public float MinBoxConfidence = 0.3f;

    [Tooltip("Detection results")]
    public IEnumerable<YOLOHandler.ResultBox> results;

    NNHandler nn;
    YOLOHandler yolo;

    WebCamTexture camTexture;
    [HideInInspector]
    public Texture2D displayingTex;
    Texture2D tempTex;

    TextureScaler textureScaler;

    public string[] classesNames;
    OnGUICanvasRelativeDrawer relativeDrawer;

    Color[] colorArray = new Color[] { Color.red, Color.green, Color.blue, Color.cyan, Color.magenta, Color.yellow };

    void Start()
    {
        //var dev = SelectCameraDevice();
        camTexture = new WebCamTexture();
        camTexture.Play();
        //displayingTex = new Texture2D(camTexture.width, camTexture.height);

        nn = new NNHandler(modelFile);
        yolo = new YOLOHandler(nn);

        /*for (int i = 0; i < nn.model.inputs[0].shape.Length; i++)
        {
            Debug.Log("Shape data " + i + ": " + nn.model.inputs[0].shape[i]);
        }*/
        textureScaler = new TextureScaler(416, 416);
        tempTex = new Texture2D(camTexture.width, camTexture.height);

        relativeDrawer = GetComponent<OnGUICanvasRelativeDrawer>();
        relativeDrawer.relativeObject = imageRenderer.GetComponent<RectTransform>();

        classesNames = classesFile.text.Split(',');

        StartCoroutine(MLUpdate());
    }

    IEnumerator MLUpdate()
    {
        // 10fps
        WaitForSeconds wait = new WaitForSeconds(0.1f);
        while (true)
        {
            tempTex.UpdateExternalTexture(camTexture.GetNativeTexturePtr());
            //tempTex.SetPixels32(camTexture.GetPixels32());
            //tempTex.Apply();

            //RotateTexture(tempTex, true);
            CaptureAndPrepareTexture(camTexture, ref displayingTex);

            var boxes = yolo.Run(displayingTex);
            /*if(boxes.Count > 0)
                Debug.Log("Found boxes " + boxes.Count);*/
            DrawResults(boxes, displayingTex);
            //tempTex.SetPixels(camTexture.GetPixels());
            imageRenderer.texture = tempTex;

            yield return wait;
        }
    }

    private void OnDestroy()
    {
        nn.Dispose();
        yolo.Dispose();
        textureScaler.Dispose();

        camTexture.Stop();
    }

    private void CaptureAndPrepareTexture(WebCamTexture camTexture, ref Texture2D tex)
    {
        Profiler.BeginSample("Texture processing");
        //tex.UpdateExternalTexture(camTexture.GetNativeTexturePtr());
        TextureCropTools.CropToSquare(camTexture, ref tex);
        textureScaler.Scale(tex);
        Profiler.EndSample();
    }

    private void DrawResults(IEnumerable<YOLOHandler.ResultBox> results, Texture2D img)
    {
        relativeDrawer.Clear();
        this.results = results;
        //results.ForEach(box => DrawBox(box, displayingTex));
    }

    /// <summary>
    /// Return first backfaced camera name if avaible, otherwise first possible
    /// </summary>
    string SelectCameraDevice()
    {
        if (WebCamTexture.devices.Length == 0)
            throw new Exception("Any camera isn't avaible!");

        foreach (var cam in WebCamTexture.devices)
        {
            if (!cam.isFrontFacing)
                return cam.name;
        }
        return WebCamTexture.devices[0].name;
    }

    Texture2D rotatedTexture;

    Texture2D RotateTexture(Texture2D originalTexture, bool clockwise)
    {
        Color32[] original = originalTexture.GetPixels32();
        Color32[] rotated = new Color32[original.Length];
        int w = originalTexture.width;
        int h = originalTexture.height;

        int iRotated, iOriginal;

        for (int j = 0; j < h; ++j)
        {
            for (int i = 0; i < w; ++i)
            {
                iRotated = (i + 1) * h - j - 1;
                iOriginal = clockwise ? original.Length - 1 - (j * w + i) : j * w + i;
                rotated[iRotated] = original[iOriginal];
            }
        }

        if (rotatedTexture == null || rotatedTexture.width != originalTexture.width || rotatedTexture.height != originalTexture.height)
            rotatedTexture = new Texture2D(h, w);

        rotatedTexture.SetPixels32(rotated);
        rotatedTexture.Apply();
        return rotatedTexture;
    }

}
