﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothTransform : MonoBehaviour
{
    [Header("Transitions")]
    public float transitionSpeed;

    [Header("Visibility")]
    public float hideTimeout;

    Vector3 targetPosition;
    Vector2 targetScale;

    private void Update()
    {
        if (transform.position == Vector3.zero)
        {
            transform.position = targetPosition;
            return;
        }

        transform.position = Vector3.Lerp(transform.position, targetPosition, Time.deltaTime * transitionSpeed);
        transform.localScale = Vector3.Lerp(transform.localScale, targetScale, Time.deltaTime * transitionSpeed / 2);
    }

    public void SetTargetPosition(Vector3 targetPosition)
    {
        this.targetPosition = targetPosition;

        ShowDisplay();
    }

    public void SetTargetScale(Vector2 targetScale)
    {
        this.targetScale = targetScale;

        ShowDisplay();
    }

    public void ShowDisplay()
    {
        StopAllCoroutines();

        gameObject.SetActive(true);
    }

    public IEnumerator HideDisplay()
    {
        yield return new WaitForSecondsRealtime(hideTimeout);

        gameObject.SetActive(false);
    }
}
