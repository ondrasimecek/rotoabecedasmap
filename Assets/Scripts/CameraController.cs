﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This component tests getting the latest camera image
/// and converting it to RGBA format. If successful,
/// it displays the image on the screen as a RawImage
/// and also displays information about the image.
/// 
/// This is useful for computer vision applications where
/// you need to access the raw pixels from camera image
/// on the CPU.
/// 
/// This is different from the ARCameraBackground component, which
/// efficiently displays the camera image on the screen. If you
/// just want to blit the camera texture to the screen, use
/// the ARCameraBackground, or use Graphics.Blit to create
/// a GPU-friendly RenderTexture.
/// 
/// In this example, we get the camera image data on the CPU,
/// convert it to an RGBA format, then display it on the screen
/// as a RawImage texture to demonstrate it is working.
/// This is done as an example; do not use this technique simply
/// to render the camera image on screen.
/// </summary>
public class CameraController : MonoBehaviour
{
    public Text m_ImageInfo;

    /*[SerializeField]
    ARCameraManager cameraManager;

    [SerializeField]
    ARRaycastManager raycastManager;*/

    /// <summary>
    /// The UI Text used to display information about the image on screen.
    /// </summary>

    public bool showImageInfo;
    public Text imageInfo
    {
        get { return m_ImageInfo; }
        set { m_ImageInfo = value; }
    }

    Texture2D m_Texture;
    public RawImage imageForTexture;
    //RenderTexture renderTexture;
    //ARSessionOrigin arOrigin;

    WebCamTexture webCamTexture;

    void Start()
    {
        mainCamera = Camera.main;

        letterHint.gameObject.SetActive(false);
        if (letters.Length == 1)
            letterHint.sprite = letters[0].letterHint;

        //cameraManager.frameReceived += OnCameraFrameReceived;

        InitTF();
        //InitIndicator();

        StartCoroutine(InitDeviceCamera());
    }

    private IEnumerator InitDeviceCamera()
    {
        yield return Application.RequestUserAuthorization(UserAuthorization.WebCam);

        if (!Application.HasUserAuthorization(UserAuthorization.WebCam))
        {
            Debug.Log("Waiting for permission");
            yield return null;
        }

        bool webcamWorking = false;
        while (!webcamWorking)
        {
            webCamTexture = new WebCamTexture(Screen.width / 2, Screen.height / 2, 15);
            webCamTexture.Play();

            Color firstPixel = webCamTexture.GetPixel(0, 0);
            //Debug.Log("First pixel " + firstPixel);

            if(firstPixel == null || firstPixel == new Color(0, 0, 0, 0))
            {
                //Debug.Log("Trying again");
                yield return null;
                continue;
            }
            else
            {
                //Debug.Log("Ready to start");
                webcamWorking = true;
            }
        }

        //renderTexture = new RenderTexture(Screen.width / 2, Screen.height / 2, 0);

        StartCoroutine(ScheduledUpdate());
    }

    private IEnumerator ScheduledUpdate()
    {
        while (true)
        {

            //Debug.Log("Playing " + webCamTexture.isPlaying + " | Updated " + webCamTexture.didUpdateThisFrame);

            if (!webCamTexture.isPlaying)
                yield break;

            if (showImageInfo)
            {
                // Display some information about the camera image
                m_ImageInfo.text = string.Format(
                    "Image info:\n\twidth: {0}\n\theight: {1}\n\tupdated: {2}\n\tmirror: {3}\n\tangle: {4}",
                    webCamTexture.width, webCamTexture.height, webCamTexture.didUpdateThisFrame, webCamTexture.videoVerticallyMirrored, webCamTexture.videoRotationAngle);
            }

            // Choose an RGBA format.
            // See CameraImage.FormatSupported for a complete list of supported formats.
            var format = TextureFormat.RGBA32;

            if (m_Texture == null || m_Texture.width != webCamTexture.width || m_Texture.height != webCamTexture.height)
                m_Texture = new Texture2D(webCamTexture.width, webCamTexture.height, format, false);
            
            m_Texture.SetPixels32(webCamTexture.GetPixels32());
            m_Texture.Apply(true);

            //Graphics.Blit(rotatedTexture, renderTexture);

            imageForTexture.texture = RotateTexture(m_Texture, true);

            //Texture2D flippedTexture = FlipTexture(m_Texture);

            // Run TensorFlow inference on the texture
            //RunTF(m_Texture);

            yield return new WaitForSeconds(0.07f);

        }
    }

    Texture2D rotatedTexture;
    Texture2D RotateTexture(Texture2D originalTexture, bool clockwise)
    {
        Color32[] original = originalTexture.GetPixels32();
        Color32[] rotated = new Color32[original.Length];
        int w = originalTexture.width;
        int h = originalTexture.height;

        int iRotated, iOriginal;

        for (int j = 0; j < h; ++j)
        {
            for (int i = 0; i < w; ++i)
            {
                iRotated = (i + 1) * h - j - 1;
                iOriginal = clockwise ? original.Length - 1 - (j * w + i) : j * w + i;
                rotated[iRotated] = original[iOriginal];
            }
        }

        if(rotatedTexture == null || rotatedTexture.width != originalTexture.width || rotatedTexture.height != originalTexture.height)
            rotatedTexture = new Texture2D(h, w);

        rotatedTexture.SetPixels32(rotated);
        rotatedTexture.Apply();
        return rotatedTexture;
    }

    /*Texture2D FlipTexture(Texture2D original)
    {
        Texture2D flipped = new Texture2D(original.width, original.height);

        int xN = original.width;
        int yN = original.height;

        for (int i = 0; i < xN; i++)
        {
            for (int j = 0; j < yN; j++)
            {
                flipped.SetPixel(xN - i - 1, j, original.GetPixel(i, j));
            }
        }
        flipped.Apply();

        return flipped;
    }*/

    void OnDisable()
    {
        //cameraManager.frameReceived -= OnCameraFrameReceived;

        //CloseTF();
    }

    /*XRCameraImage image;

    unsafe void OnCameraFrameReceived(ARCameraFrameEventArgs eventArgs)
    {
        // Attempt to get the latest camera image. If this method succeeds,
        // it acquires a native resource that must be disposed (see below).
        if (!cameraManager.subsystem.TryGetLatestImage(out image))
            return;

        // Display some information about the camera image
        m_ImageInfo.text = string.Format(
            "Image info:\n\twidth: {0}\n\theight: {1}\n\tplaneCount: {2}\n\ttimestamp: {3}\n\tformat: {4}",
            image.width, image.height, image.planeCount, image.timestamp, image.format);

        // Choose an RGBA format.
        // See CameraImage.FormatSupported for a complete list of supported formats.
        var format = TextureFormat.RGBA32;

        if (m_Texture == null || m_Texture.width != image.width || m_Texture.height != image.height)
            m_Texture = new Texture2D(image.width, image.height, format, false);

        // Convert the image to format, flipping the image across the Y axis.
        // We can also get a sub rectangle, but we'll get the full image here.
        var conversionParams = new XRCameraImageConversionParams(image, format, CameraImageTransformation.None);

        // Texture2D allows us write directly to the raw texture data
        // This allows us to do the conversion in-place without making any copies.
        var rawTextureData = m_Texture.GetRawTextureData<byte>();
        try
        {
            image.Convert(conversionParams, new IntPtr(rawTextureData.GetUnsafePtr()), rawTextureData.Length);
        }
        finally
        {
            // We must dispose of the CameraImage after we're finished
            // with it to avoid leaking native resources.
            image.Dispose();
        }

        // Apply the updated texture data to our texture
        m_Texture.Apply();

        // Run TensorFlow inference on the texture
        RunTF(m_Texture);
    }*/

    [Header("Model settings")]
    public TextAsset model;
    public TextAsset labels;

    [Header("Display")]
    public bool showBoundingBoxes;
    public SmoothTransform display;
    public Image displayImage;
    public Image letterHint;
    public AudioSource audioSource;

    [Header("Letters")]
    public LetterData[] letters;

    private ImageData selectedImage;
    private LetterData recognizedLetter;
    private int resetImageCountdown;

    //Classifier classifier;
    //Detector detector;
    //bool detectorInitialized = false;

    private Camera mainCamera;
    //private IList outputs;
    //private GameObject apple;

    public void InitTF()
    {
        // MobileNet
        //classifier = new Classifier(model, labels, output: "MobilenetV1/Predictions/Reshape_1");

        // SSD MobileNet
        /*detector = new Detector(model, labels, 
                                input: "input");*/

        // Tiny YOLOv2
        /*detector = new Detector(model, labels, DetectionModels.YOLO,
                                width: 192,
                                height: 192,
                                mean: 0,
                                std: 255);*/
    }
    
    /*public void RunTF(Texture2D texture)
    {
        //Debug.Log("Detector " + detector + " | DI " + detectorInitialized);
        if (detector == null && !detectorInitialized)
        {
            InitTF();
            detectorInitialized = true;
        }*/
        /*else
        {
            InitTF();
        }
        Debug.Log("Detector " + detector + " | DI " + detectorInitialized);*/

        /*if (outputs == null)
            outputs = new List<Dictionary<string, object>>();

        // MobileNet
        //outputs = classifier.Classify(texture, angle: 90, threshold: 0.05f);

        // SSD MobileNet
        //outputs = detector.Detect(m_Texture, angle: 90, threshold: 0.6f);

        // Tiny YOLOv2
        outputs = detector.Detect(m_Texture, angle: 90, threshold: 0.25f);

        //Debug.Log("Outputs count " + outputs.Count);

        // hint visible only for single letters
        if (letters.Length == 1)
        {
            letterHint.gameObject.SetActive(!display.gameObject.activeSelf);
        }

        if (outputs.Count < 1)
        {
            StartCoroutine(display.HideDisplay());
            resetImageCountdown--;
            return;
        }

        // show graphics
        // only first result
        //for (int i = 0; i < outputs.Count; i++)
        //{

        var output = outputs[0] as Dictionary<string, object>;
        //Debug.Log("Detected " + output["detectedClass"]);
        // try all letters
        for (int i = 0; i < letters.Length; i++)
        {
            if (output["detectedClass"].Equals(letters[i].name))
            {
                if (recognizedLetter != letters[i])
                {
                    resetImageCountdown = -1;
                    recognizedLetter = letters[i];
                }

                //DrawApple(output["rect"] as Dictionary<string, float>);
                ShowGraphics(letters[i], output["rect"] as Dictionary<string, float>);
            }
        }

        //}
    }*/

    /*public void CloseTF()
    {
        //classifier.Close();
        detector.Close();
    }*/

    public void OnGUI()
    {
        if (!showBoundingBoxes)
            return;

        /*if (outputs != null)
        {
            // Classification
            //Utils.DrawOutput(outputs, new Vector2(20, 20), Color.red);

            // Object detection
            //Utils.DrawOutput(outputs, Screen.width, Screen.height, Color.yellow);
        }*/
    }

    private void ShowGraphics(LetterData letter, Dictionary<string, float> rect)
    {
        //Debug.Log("Showing graphics");
        //Debug.Log("Rect X:" + rect["x"] + " | Y:" + rect["y"] + " | W:" + rect["w"] + " | H:" + rect["h"]);

        var xMin = 1 - rect["x"];
        var yMin = 1 - rect["y"];
        var xMax = 1 - rect["x"] - rect["w"];
        var yMax = 1 - rect["y"] - rect["h"];

        //var pos = GetPosition((xMin + xMax) / 2 * Screen.width, (yMin + yMax) / 2 * Screen.height);
        Vector3 pos = mainCamera.ScreenToWorldPoint(new Vector3((xMin + xMax) / 2 * Screen.width, (yMin + yMax) / 2 * Screen.height, 10));

        //Debug.Log("Position " + pos);

        display.SetTargetPosition(pos);

        ImageData imageData = letter.imageData[UnityEngine.Random.Range(0, letter.imageData.Length)];
        if (resetImageCountdown < 0 || selectedImage == null)
        {
            selectedImage = imageData;
            resetImageCountdown = 10;
        }
        else
        {
            imageData = selectedImage;
            resetImageCountdown = 10;
        }

        displayImage.sprite = imageData.sprite;
        audioSource.clip = imageData.sound;
    }

    public void PlaySound()
    {
        audioSource.Stop();
        audioSource.Play();
    }
}
